import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.BodyPart;
import southpark.figure.pool.BodyPartPool;
import southpark.figure.utils.Utils;

public class BodyPartPoolTest {

	public static final String TEST_DIR = "resources/test";

	private static List<FigureElement> elements = new ArrayList<>();
	private static BodyPartPool testPool;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Started testing Pool");
		// BodyPartPool's type doesn't matter.
		testPool = new BodyPartPool(TEST_DIR, BodyPart.HAT);
		elements = Utils.fetchImages(TEST_DIR);
	}
	
	@Before
	public void resetPool() {
		// BodyPartPool's type doesn't matter.
		testPool = new BodyPartPool(TEST_DIR, BodyPart.HAT);
	}
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("Ended testing Pool");
	}

	@Test
	public void testSize() {
		assertEquals(testPool.getSize(), elements.size());
	}

	@Test
	public void testNextFromBeginning() {
		FigureElement f1 = testPool.getNext();
		FigureElement f2 = elements.get(1);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}

	@Test
	public void testNextFromFirst() {
		testPool.getNext();
		FigureElement f1 = testPool.getNext();
		FigureElement f2 = elements.get(2);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
	
	@Test
	public void testNextFromTheLast() {
		testPool.getNext();
		testPool.getNext();
		FigureElement f1 = testPool.getNext();
		FigureElement f2 = elements.get(0);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
	
	@Test
	public void testNextStartingFromTheLast() {
		testPool.getNext();
		testPool.getNext();
		testPool.getNext();
		FigureElement f1 = testPool.getNext();
		FigureElement f2 = elements.get(1);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
	
	@Test
	public void testPreviousFromBeginning() {
		resetPool();
		FigureElement f1 = testPool.getPrevious();
		FigureElement f2 = elements.get(2);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}

	@Test
	public void testPreviousFromFirst() {
		testPool.getPrevious();
		FigureElement f1 = testPool.getPrevious();
		FigureElement f2 = elements.get(1);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
	
	@Test
	public void testPreviousFromTheLast() {
		testPool.getPrevious();
		testPool.getPrevious();
		FigureElement f1 = testPool.getPrevious();
		FigureElement f2 = elements.get(0);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
	
	@Test
	public void testPreviousStartingFromTheFirst() {
		FigureElement f1 = testPool.getPrevious();
		FigureElement f2 = elements.get(2);
		assertTrue(f1.getElementImage().getPath().equals(f2.getElementImage().getPath()));
	}
}
