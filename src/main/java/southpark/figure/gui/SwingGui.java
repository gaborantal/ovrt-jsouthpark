package southpark.figure.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.lang.WordUtils;

import southpark.figure.controller.IController;
import southpark.figure.enums.BodyPart;
import southpark.figure.enums.Command;
import southpark.figure.utils.Constants;
import southpark.figure.utils.Utils;

/**
 * Gui class
 */
public class SwingGui {

	/**
	 * Frame
	 */
	public JFrame frame;

	/**
	 * Only gui instance
	 */
	private static SwingGui instance;
	private IController controller;
	private FigurePanel figurePanel;
	private FigureImagePanel figureImagePanel;
	private FigureStatisticsPanel figureStatisticsPanel;
	private JPanel settingsFrame;
	private String savePathString = "temp.jpg";
	private String loadPathString = "temp.json";
	private JMenuBar menuBar;
	private Map<BodyPart, JCheckBox> checkboxes;
	private Map<BodyPart, JRadioButton> radios;
	private ButtonGroup radiosGroup;

	private JTextField step = new JTextField();

	/**
	 * Class constructor.
	 */
	protected SwingGui() {
		frame = new JFrame();
		figurePanel = new FigurePanel();
		figureImagePanel = new FigureImagePanel();
		figureStatisticsPanel = new FigureStatisticsPanel();
		settingsFrame = new JPanel();
		checkboxes = new HashMap<>();
		radios = new HashMap<>();
		radiosGroup = new ButtonGroup();
		menuBar = new JMenuBar();

		frame.setSize(980, 610);

		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		int framePosX = (screenDim.width / 2) - (frame.getSize().width / 2);
		int framePosY = (screenDim.height / 2) - (frame.getSize().height / 2);

		frame.setLocation(framePosX, framePosY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 2, 10, 0));

		addMenuBar();

		settingsFrame.setLayout(new GridLayout(26, 1, 0, 0));

		for (BodyPart currentPart : BodyPart.values()) {
			addBodyPartPanelToSettingsPanel(currentPart);
		}

		addMoveSettingsToSettingsPanel();

		figurePanel.setBounds(0, 0, 490, 550);
		figureImagePanel.setBounds(0, 0, 490, 550);

		JPanel figureStatisticsPanelContainer = new JPanel(new BorderLayout());
		figureStatisticsPanelContainer.setBounds(0, 0, 490, 550);
		figureStatisticsPanelContainer.setOpaque(false);
		figureStatisticsPanelContainer.add(figureStatisticsPanel, BorderLayout.SOUTH);

		figurePanel.add(figureImagePanel, 0, 0);
		figurePanel.add(figureStatisticsPanelContainer, 1, 0);

		frame.getContentPane().add(figurePanel);
		frame.getContentPane().add(settingsFrame);
		frame.setVisible(true);
	}

	/**
	 * Get instance.
	 */
	public static synchronized SwingGui getInstance() {
		if (instance == null) {
			instance = new SwingGui();
		}
		return instance;
	}

	/**
	 * Set controller.
	 */
	public void setController(IController c) {
		this.controller = c;
	}

	/**
	 * Get figure panel.
	 */
	public FigurePanel getFigurePanel() {
		return figurePanel;
	}

	/**
	 * Get figure image panel.
	 */
	public FigureImagePanel getFigureImagePanel() {
		return figureImagePanel;
	}

	/**
	 * Get figure statistics panel.
	 */
	public FigureStatisticsPanel getFigureStatisticsPanel() {
		return figureStatisticsPanel;
	}

	/**
	 * Get checkboxes.
	 */
	public Map<BodyPart, JCheckBox> getCheckboxes() {
		return checkboxes;
	}

	/**
	 * Get radios.
	 */
	public Map<BodyPart, JRadioButton> getRadios() {
		return radios;
	}

	/**
	 * Add body part panel to settings frame.
	 */
	private void addBodyPartPanelToSettingsPanel(BodyPart bodyPart) {
		addBodyPartPanelToSettingsPanel(WordUtils.capitalize(bodyPart.name().toLowerCase(Locale.getDefault())), bodyPart);
	}

	/**
	 * Add body part panel to settings panel.
	 */
	private void addBodyPartPanelToSettingsPanel(String label, BodyPart bodyPart) {
		JLabel settingsLabel = new JLabel(label);
		Box settingsPanel = Box.createHorizontalBox();
		JPanel buttonPanel = new JPanel(new GridLayout(1, 2, 5, 0));

		JButton buttonPrev = new JButton("< Previous");
		buttonPrev.addActionListener(click -> {
			controller.move(bodyPart, Command.LEFT);
		});

		JButton buttonNext = new JButton("Next >");
		buttonNext.addActionListener(click -> {
			controller.move(bodyPart, Command.RIGHT);
		});

		buttonPanel.add(buttonPrev);
		buttonPanel.add(buttonNext);

		settingsPanel.add(buttonPanel);
		settingsPanel.add(Box.createHorizontalStrut(20));

		JCheckBox checkbox = new JCheckBox("Randomize");
		checkbox.setSelected(true);
		checkboxes.put(bodyPart, checkbox);

		settingsPanel.add(checkbox);
		settingsPanel.add(Box.createHorizontalStrut(20));

		JRadioButton radio = new JRadioButton("Move");
		// Select background by default.
		if (bodyPart == BodyPart.BACKGROUND) {
			radio.setSelected(true);
		}
		radiosGroup.add(radio);
		radios.put(bodyPart, radio);

		settingsPanel.add(radio);

		settingsFrame.add(settingsLabel);
		settingsFrame.add(settingsPanel);
	}

	/**
	 * Add move settings to settings panel.
	 */
	private void addMoveSettingsToSettingsPanel() {
		JLabel lblSetBackground = new JLabel("Move settings");
		JPanel buttonPanel = new JPanel(new GridLayout(1, 7, 5, 0));

		JButton btnLeft = new JButton("Left");
		btnLeft.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.LEFT);
		});

		JButton btnRight = new JButton("Right");
		btnRight.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.RIGHT);
		});

		JButton btnUp = new JButton("Up");
		btnUp.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.UP);
		});

		JButton btnDown = new JButton("Down");
		btnDown.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.DOWN);
		});

		JButton btnSizeUp = new JButton("+");
		btnSizeUp.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.SCALE_UP);
		});

		JButton btnSizeDown = new JButton("-");
		btnSizeDown.addActionListener(click -> {
			int num = Utils.parseInteger(step);
			controller.moveSelectedBodyPartOnImage(num, Command.SCALE_DOWN);
		});

		buttonPanel.add(step);
		buttonPanel.add(btnLeft);
		buttonPanel.add(btnRight);
		buttonPanel.add(btnUp);
		buttonPanel.add(btnDown);
		buttonPanel.add(btnSizeUp);
		buttonPanel.add(btnSizeDown);

		settingsFrame.add(lblSetBackground);
		settingsFrame.add(buttonPanel);
	}

	/**
	 * Add menu bar.
	 */
	private void addMenuBar() {
		JMenuItem resetFigureMenuItem = new JMenuItem("Reset figure to default", KeyEvent.VK_N);
		resetFigureMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		resetFigureMenuItem.addActionListener(event -> {
			controller.resetFigure();
		});

		JMenuItem randomFigureMenuItem = new JMenuItem("Get a random figure", KeyEvent.VK_R);
		randomFigureMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		randomFigureMenuItem.addActionListener(event -> {
			boolean result = controller.randomFigure();
			handleControllerEvent(result, null, "Failed to get a randomized figure");
		});

		JMenuItem saveFigureMenuItem = new JMenuItem("Save figure to image", KeyEvent.VK_S);
		saveFigureMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		saveFigureMenuItem.addActionListener(event -> {
			saveAsImage();
		});

		JMenuItem saveMelancholicMenuItem = new JMenuItem("Save figure to melancholic image", KeyEvent.VK_M);
		saveMelancholicMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
		saveMelancholicMenuItem.addActionListener(event -> {
			saveAsMelancholicImage();
		});

		JMenuItem saveToFigureMenuItem = new JMenuItem("Save figure to figure", KeyEvent.VK_S);
		saveToFigureMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.SHIFT_MASK | ActionEvent.CTRL_MASK));
		saveToFigureMenuItem.addActionListener(event -> {
			saveAsFigure();
		});

		JMenuItem saveToCSVMenuItem = new JMenuItem("Save figure to CSV", KeyEvent.VK_V);
		saveToCSVMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
		saveToCSVMenuItem.addActionListener(event -> {
			saveAsCSV();
		});

		JMenuItem loadFigureMenuItem = new JMenuItem("Load figure", KeyEvent.VK_L);
		loadFigureMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		loadFigureMenuItem.addActionListener(event -> {
			loadFromFigure();
		});

		JMenuItem undoMenuItem = new JMenuItem("Undo", KeyEvent.VK_Z);
		undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
		undoMenuItem.addActionListener(event -> {
			controller.undo();
		});

		JMenuItem redoMenuItem = new JMenuItem("Redo", KeyEvent.VK_Y);
		redoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
		redoMenuItem.addActionListener(event -> {
			controller.redo();
		});

		JMenuItem exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_X);
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK | ActionEvent.SHIFT_MASK | ActionEvent.CTRL_MASK));
		exitMenuItem.addActionListener(event -> {
			System.exit(0); // NOPMD
		    });

		JMenuItem getCartmanMenuItem = new JMenuItem("Get Cartman", KeyEvent.VK_C);
		getCartmanMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.SHIFT_MASK));
		getCartmanMenuItem.addActionListener(event -> {
			controller.getCartman();
		});

		JMenuItem getKennyMenuItem = new JMenuItem("Get Kenny", KeyEvent.VK_K);
		getKennyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, ActionEvent.SHIFT_MASK));
		getKennyMenuItem.addActionListener(event -> {
			controller.getKenny();
		});

		JMenuItem getStanMenuItem = new JMenuItem("Get Stan", KeyEvent.VK_S);
		getStanMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.SHIFT_MASK));
		getStanMenuItem.addActionListener(event -> {
			controller.getStan();
		});

		JMenuItem getKyleMenuItem = new JMenuItem("Get Kyle", KeyEvent.VK_Y);
		getKyleMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.SHIFT_MASK));
		getKyleMenuItem.addActionListener(event -> {
			controller.getKyle();
		});

		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.add(saveFigureMenuItem);
		fileMenu.add(saveToFigureMenuItem);
		fileMenu.add(saveMelancholicMenuItem);
		fileMenu.add(saveToCSVMenuItem);
		fileMenu.add(loadFigureMenuItem);
		fileMenu.add(exitMenuItem);

		JMenu figureMenu = new JMenu("Figure");
		figureMenu.setMnemonic(KeyEvent.VK_I);
		figureMenu.add(undoMenuItem);
		figureMenu.add(redoMenuItem);
		figureMenu.add(resetFigureMenuItem);
		figureMenu.add(randomFigureMenuItem);

		JMenu charactersMenu = new JMenu("Character");
		figureMenu.setMnemonic(KeyEvent.VK_C);

		charactersMenu.add(getCartmanMenuItem);
		charactersMenu.add(getKennyMenuItem);
		charactersMenu.add(getStanMenuItem);
		charactersMenu.add(getKyleMenuItem);

		JMenu drawPanel = new JMenu("Draw");
		JMenuItem draw = new JMenuItem("Draw");
		draw.addActionListener(event -> {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					new DrawPanel(controller).createAndShowGUI();
				}
			});
		});
		drawPanel.add(draw);

		menuBar.add(fileMenu);
		menuBar.add(figureMenu);
		menuBar.add(charactersMenu);
		menuBar.add(drawPanel);

		frame.setJMenuBar(menuBar);
	}

	/**
	 * Save as image.
	 */
	private void saveAsImage() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle(Constants.SPECIFY_A_FILE_TO_SAVE);

		int userSelection = fileChooser.showSaveDialog(settingsFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			savePathString = fileToSave.getAbsolutePath();
			handleControllerEvent(controller.saveImageAsPNG(savePathString), Constants.IMAGE_CREATED_SUCCESFULLY, Constants.FAILED_TO_WRITE_IMAGE);
		}
	}

	/**
	 * Save as black & white image.
	 */
	private void saveAsMelancholicImage() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle(Constants.SPECIFY_A_FILE_TO_SAVE);

		int userSelection = fileChooser.showSaveDialog(settingsFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			savePathString = fileToSave.getAbsolutePath();
			handleControllerEvent(controller.saveImageAsMelancholicPNG(savePathString), Constants.IMAGE_CREATED_SUCCESFULLY, Constants.FAILED_TO_WRITE_IMAGE);
		}
	}

	/**
	 * Save as figure.
	 */
	private void saveAsFigure() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle(Constants.SPECIFY_A_FILE_TO_SAVE);

		int userSelection = fileChooser.showSaveDialog(settingsFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			savePathString = fileToSave.getAbsolutePath();
			handleControllerEvent(controller.saveImageAsFigure(savePathString), "Figure file created successfully", "Failed to write figure file!");
		}
	}

	/**
	 * Save as CSV.
	 */
	private void saveAsCSV() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle(Constants.SPECIFY_A_FILE_TO_SAVE);

		int userSelection = fileChooser.showSaveDialog(settingsFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			savePathString = fileToSave.getAbsolutePath();
			handleControllerEvent(controller.saveImageAsCSV(savePathString), "File created successfully", "Failed to write CSV file!");
		}
	}

	/**
	 * Load from figure.
	 */
	private void loadFromFigure() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileFilter() {

			@Override
			public String getDescription() {
				return "Own Figure extension";
			}

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().toLowerCase(Locale.getDefault()).endsWith(Constants.SAVE_SUFFIX);
			}
		});

		fileChooser.setDialogTitle("Specify a saved figure");

		int userSelection = fileChooser.showOpenDialog(settingsFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			loadPathString = fileToSave.getAbsolutePath();
			handleControllerEvent(controller.loadFigureFromFile(loadPathString), null, "Failed to load figure!");
		}
	}

	/**
	 * Handle controller event.
	 */
	private void handleControllerEvent(boolean event, String infoSuccess, String infoFail) {
		if (event) {
			if (infoSuccess != null) {
				JOptionPane.showMessageDialog(null, infoSuccess, "Succeeded!", JOptionPane.INFORMATION_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, infoFail, "Failed!", JOptionPane.ERROR_MESSAGE);
		}
	}

}
