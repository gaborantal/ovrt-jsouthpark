package southpark.figure.gui;

import java.util.List;

import javax.swing.JLayeredPane;

import southpark.figure.entity.Figure;
import southpark.figure.observer.IObservable;
import southpark.figure.observer.IObserver;

/**
 * Figure's image panel
 */
public class FigureImagePanel extends JLayeredPane implements IObserver {

	private static final long serialVersionUID = -2406959356902845898L;

	/**
	 * Update figure frame.
	 */
	public void update(IObservable observable) {
		if (observable instanceof Figure) {
			removeAll();

			Figure figure = (Figure) observable;
			List<ImagePanel> figureImages = figure.getImageParts();
			figureImages.forEach(imagePart -> {
				add(imagePart);
			});
		}
	}

}
