package southpark.figure.strategy;

/**
 * Saving strategy
 */
public interface ISaveStrategy {
	
	/**
	 * Save operation
	 * 
	 * @return true if file written successfully
	 */
	public abstract boolean save();

	/**
	 * Check's file extension
	 * 
	 * @return true, if file name is valid, false, otherwise
	 */
	public abstract boolean checkExtension();

	/**
	 * Modify file extension if it's not meet the checkExtension()'s criterai
	 */
	public abstract void modifyExtension();

	/**
	 * Set the name of the file, where the save action will be executed
	 * 
	 * @param fileName
	 *            filename to save
	 */
	public void setFileName(String fileName);

}
