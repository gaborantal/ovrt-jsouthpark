package southpark.figure.strategy;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

import southpark.figure.gui.FigureImagePanel;
import southpark.figure.utils.Constants;
import southpark.figure.utils.Utils;

/**
 * Save as melancholic image strategy
 */
public class SaveAsMelancholicImageStrategy implements ISaveStrategy {

	private static final Logger log = Logger.getLogger(SaveAsMelancholicImageStrategy.class.getCanonicalName());
	private String fileName;
	private FigureImagePanel figureFrame;

	/**
	 * Class constructor.
	 */
	public SaveAsMelancholicImageStrategy(FigureImagePanel figureFrame) {
		this.figureFrame = figureFrame;
	}

	/**
	 * Set file name.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Save.
	 */
	public boolean save() {
		File outputfile = new File(fileName);

		BufferedImage bufferedImage = Utils.createImage(figureFrame);

		ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
		op.filter(bufferedImage, bufferedImage);

		RescaleOp rescaleOp = new RescaleOp(0.75f, 20, null);
		rescaleOp.filter(bufferedImage, bufferedImage);

		try {
			ImageIO.write(bufferedImage, Constants.IMAGE_EXTENSION, outputfile);
			return true;
		} catch (IOException e) {
			log.error("Cannot save image! " + e.getMessage());
			return false;
		}
	}

	/**
	 * Check extension.
	 */
	public boolean checkExtension() {
		return fileName.endsWith(Constants.IMG_SUFFIX);
	}

	/**
	 * Modify extension.
	 */
	public void modifyExtension() {
		fileName = fileName + Constants.IMG_SUFFIX;
	}

}
