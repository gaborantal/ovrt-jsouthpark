package southpark.figure.strategy;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import southpark.figure.entity.IFigure;
import southpark.figure.utils.Constants;

import com.cedarsoftware.util.io.JsonWriter;

/**
 * Save as figure strategy implementation
 */
public class SaveAsFigureStrategy implements ISaveStrategy {

	private static final Logger log = Logger.getLogger(SaveAsFigureStrategy.class.getCanonicalName());
	private String fileName;
	private IFigure figure;

	/**
	 * Class constructor.
	 */
	public SaveAsFigureStrategy(IFigure figure) {
		this.figure = figure;
	}
	
	/**
	 * Set file name.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Save.
	 */
	public boolean save() {
		String json = JsonWriter.objectToJson(figure);
		
		try {
			FileUtils.writeStringToFile(new File(fileName), json);
			return true;
		} catch (IOException e) {
			log.error("Writing to file failed: " + e);
			return false;
		}
	}

	/**
	 * Check extension.
	 */
	public boolean checkExtension() {
		return fileName.endsWith(Constants.SAVE_SUFFIX);
	}

	/**
	 * Modify extension.
	 */
	public void modifyExtension() {
		fileName = fileName + Constants.SAVE_SUFFIX;
	}
	
}
