package southpark.figure.strategy;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import southpark.figure.entity.Figure;
import southpark.figure.entity.IFigure;
import southpark.figure.enums.BodyPart;
import southpark.figure.utils.Constants;

/**
 * Save as CSV implementation
 */
public class SaveAsCSVStrategy implements ISaveStrategy {

	private static final Logger log = Logger.getLogger(SaveAsCSVStrategy.class.getCanonicalName());
	private String fileName;
	private Figure figure;
	private StringBuilder stringBuilder;

	/**
	 * Class constructor.
	 */
	public SaveAsCSVStrategy(Figure figure) {
		this.figure = figure;
	}

	/**
	 * Set file name.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Save.
	 */
	public boolean save() {
		stringBuilder = new StringBuilder();
		stringBuilder.append("Body part;File name;Coolnes point; Average; Aggregated");
		stringBuilder.append(System.lineSeparator());

		int index = 0;
		int sum = 0;
		for (IFigure it : figure) {
			sum += it.getCoolness();
			stringBuilder.append(BodyPart.getElementByIndex(index++));
			stringBuilder.append(';');
			stringBuilder.append(FilenameUtils.getName(it.getElementImage().getPath()));
			stringBuilder.append(';');
			stringBuilder.append(it.getCoolness());
			stringBuilder.append(';');
			stringBuilder.append(String.valueOf((double) sum / index).replace('.', ','));
			stringBuilder.append(';');
			stringBuilder.append(sum);
			stringBuilder.append(System.lineSeparator());
		}
		stringBuilder.append(";;; ");
		stringBuilder.append(figure.getAvgCoolness());
		stringBuilder.append(";");
		stringBuilder.append(figure.getCoolness());
		stringBuilder.append(System.lineSeparator());

		try {
			FileUtils.writeStringToFile(new File(fileName), stringBuilder.toString());
			return true;
		} catch (IOException e) {
			log.error("Writing to file failed: " + e);
			return false;
		} finally {
			stringBuilder = null;
		}
	}

	/**
	 * Check extension.
	 */
	public boolean checkExtension() {
		return fileName.endsWith(Constants.CSV_SUFFIX);
	}

	/**
	 * Modify extension.
	 */
	public void modifyExtension() {
		fileName = fileName + Constants.CSV_SUFFIX;

	}

}
