package southpark.figure.strategy;

/**
 * Figure generator
 */
public class FigureGenerator {
	
	private IGenerateFigureStrategy behavior;
	
	/**
	 * Class constructor;
	 */
	public FigureGenerator(IGenerateFigureStrategy strategy) {
		this.behavior = strategy;
	}
	
	/**
	 * Get behavior.
	 */
	public IGenerateFigureStrategy getBehavior() {
		return behavior;
	}
	
	/**
	 * Set behavior.
	 */
	public void setBehavior(IGenerateFigureStrategy strategy) {
		this.behavior = strategy;
	}
	
	/**
	 * Concrete generate operation.
	 */
	public boolean concreteGenerate() {
		return behavior.generate();
	}

}
