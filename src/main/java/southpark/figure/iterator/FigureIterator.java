package southpark.figure.iterator;

import java.util.Iterator;

import southpark.figure.entity.Figure;
import southpark.figure.entity.IFigure;

/**
 * Figure iterator
 */
public class FigureIterator implements Iterator<IFigure> {

	private int counter = 0;
	private Figure container;

	/**
	 * Constructor
	 */
	public FigureIterator(Figure container) {
		super();
		this.container = container;
	}

	@Override
	public boolean hasNext() {
		return this.counter < container.getElements().size();
	}

	@Override
	public IFigure next() {
		return container.getElements().get(counter++);
	}

}
