package southpark.figure.main;

import java.awt.EventQueue;

import org.apache.log4j.Logger;

import southpark.figure.controller.Controller;
import southpark.figure.controller.IController;
import southpark.figure.enums.BodyPart;
import southpark.figure.gui.SwingGui;


/**
 * Main
 */
public class Main {

	/**
	 * Logger for main
	 */
	private static final Logger logger = Logger.getLogger(Main.class.getCanonicalName());

	/**
	 * Gui
	 */
	private static SwingGui gui;

	/**
	 * Controller
	 */
	private static IController controller;

	/**
	 * Application entry point.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BodyPart.createDirs();
					gui = SwingGui.getInstance();
					controller = new Controller(gui);
					gui.setController(controller);
					gui.frame.setVisible(true);
					controller.randomFigure();
				} catch (Exception exception) {
					logger.error("Something bad happened: " + exception);
				}
			}
		});
	}

}
