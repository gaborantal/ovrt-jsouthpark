package southpark.figure.commands;

/**
 * Command's interface
 */
public interface ICommand {

	/**
	 * Execute the command
	 */
	public void execute();

	/**
	 * Undo the commands
	 */
	public void undo();

}
