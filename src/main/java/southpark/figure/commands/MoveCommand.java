package southpark.figure.commands;

import java.awt.Point;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.Command;

public class MoveCommand implements ICommand {

	private FigureElement figureElement;
	private Command action;
	private int num;
	private Point oldLocation;

	/**
	 * Class constructor.
	 */
	public MoveCommand(FigureElement figureElement, Command action, int num) {
		this.figureElement = figureElement;
		this.action = action;
		this.num = num;
		oldLocation = figureElement.getElementImage().getLocation();
	}

	/**
	 * Execute command.
	 */
	public void execute() {
		Point newLocation;
		
		switch (action) {
			case LEFT:
				oldLocation = figureElement.getElementImage().getLocation();
				newLocation = new Point(oldLocation.x - num, oldLocation.y);
				figureElement.setElementImageLocation(newLocation);
				break;
				
			case RIGHT:
				oldLocation = figureElement.getElementImage().getLocation();
				newLocation = new Point(oldLocation.x + num, oldLocation.y);
				figureElement.setElementImageLocation(newLocation);
				break;
				
			case UP:
				oldLocation = figureElement.getElementImage().getLocation();
				newLocation = new Point(oldLocation.x, oldLocation.y - num);
				figureElement.setElementImageLocation(newLocation);
				break;
				
			case DOWN:
				oldLocation = figureElement.getElementImage().getLocation();
				newLocation = new Point(oldLocation.x, oldLocation.y + num);
				figureElement.setElementImageLocation(newLocation);
				break;
				
			default:
				break;
		}
	}

	/**
	 * Undo command.
	 */
	public void undo() {
		figureElement.setElementImageLocation(oldLocation);
	}

}
