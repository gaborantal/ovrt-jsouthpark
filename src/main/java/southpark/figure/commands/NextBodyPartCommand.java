package southpark.figure.commands;

import southpark.figure.entity.Figure;
import southpark.figure.pool.BodyPartPool;

public class NextBodyPartCommand implements ICommand {

	private BodyPartPool partPool;
	private Figure figure;

	/**
	 * Class constructor.
	 */
	public NextBodyPartCommand(Figure figure, BodyPartPool pool) {
		this.figure = figure;
		partPool = pool;
	}

	/**
	 * Execute command.
	 */
	public void execute() {
		figure.add(partPool.getType(), partPool.getNext());
	}

	/**
	 * Undo command.
	 */
	public void undo() {
		figure.add(partPool.getType(), partPool.getPrevious());
	}

}
