package southpark.figure.prototypes;

import southpark.figure.entity.Figure;
import southpark.figure.entity.FigureBuilder;
import southpark.figure.entity.FigureElement;
import southpark.figure.utils.Constants;
import southpark.figure.utils.Utils;

/**
 * Figure prototypes creator
 * 
 * with static methods
 *
 */
public class FigurePrototypes {

	private static FigurePrototypes instance;

	/**
	 * Class constructor.
	 */
	protected FigurePrototypes() {

	}

	/**
	 * Get instance.
	 */
	public synchronized FigurePrototypes getInstance() {
		if (instance == null) {
			instance = new FigurePrototypes();
		}
		return instance;
	}

	/**
	 * Create and initialize.
	 */
	public static Figure createAndInit() {
		return new FigureBuilder().build();
	}

	/**
	 * Get Cartman.
	 */
	public static Figure getCartman() {
		String shirtpath = Constants.SHIRT_DIR + "/cartman_15.png";
		String hatpath = Constants.HATS_DIR + "/cartman_20.png";
		String handpath = Constants.HANDS_DIR + "/cartman_5.png";
		String legpath = Constants.LEGS_DIR + "/cartman_4.png";
		String shoepath = Constants.SHOES_DIR + "/shoes3_1.png";
		return new FigureBuilder().withShirt(new FigureElement(shirtpath, Utils.getCoolnessFromFilename(shirtpath))).withHat(new FigureElement(hatpath, Utils.getCoolnessFromFilename(hatpath))).withHands(new FigureElement(handpath, Utils.getCoolnessFromFilename(handpath))).withLegs(new FigureElement(legpath, Utils.getCoolnessFromFilename(legpath))).withShoes(new FigureElement(shoepath, Utils.getCoolnessFromFilename(shoepath))).build();
	}

	/**
	 * Get Kenny.
	 */
	public static Figure getKenny() {
		String shirtpath = Constants.SHIRT_DIR + "/kenny_15.png";
		String hatpath = Constants.HATS_DIR + "/kenny_40.png";
		String handpath = Constants.HANDS_DIR + "/kenny_5.png";
		String legpath = Constants.LEGS_DIR + "/kenny_4.png";
		String shoepath = Constants.SHOES_DIR + "/shoes3_1.png";
		return new FigureBuilder().withShirt(new FigureElement(shirtpath, Utils.getCoolnessFromFilename(shirtpath))).withHat(new FigureElement(hatpath, Utils.getCoolnessFromFilename(hatpath))).withHands(new FigureElement(handpath, Utils.getCoolnessFromFilename(handpath))).withLegs(new FigureElement(legpath, Utils.getCoolnessFromFilename(legpath))).withShoes(new FigureElement(shoepath, Utils.getCoolnessFromFilename(shoepath))).build();
	}

	/**
	 * Get Stan.
	 */
	public static Figure getStan() {
		String shirtpath = Constants.SHIRT_DIR + "/stan_15.png";
		String hatpath = Constants.HATS_DIR + "/stan_20.png";
		String handpath = Constants.HANDS_DIR + "/stan_5.png";
		String legpath = Constants.LEGS_DIR + "/stan_4.png";
		String shoepath = Constants.SHOES_DIR + "/shoes3_1.png";
		return new FigureBuilder().withShirt(new FigureElement(shirtpath, Utils.getCoolnessFromFilename(shirtpath))).withHat(new FigureElement(hatpath, Utils.getCoolnessFromFilename(hatpath))).withHands(new FigureElement(handpath, Utils.getCoolnessFromFilename(handpath))).withLegs(new FigureElement(legpath, Utils.getCoolnessFromFilename(legpath))).withShoes(new FigureElement(shoepath, Utils.getCoolnessFromFilename(shoepath))).build();
	}

	/**
	 * Get Kyle.
	 */
	public static Figure getKyle() {
		String shirtpath = Constants.SHIRT_DIR + "/kyle_15.png";
		String hatpath = Constants.HATS_DIR + "/kyle_10.png";
		String handpath = Constants.HANDS_DIR + "/kyle_5.png";
		String legpath = Constants.LEGS_DIR + "/kyle_4.png";
		String shoepath = Constants.SHOES_DIR + "/shoes3_1.png";
		return new FigureBuilder().withShirt(new FigureElement(shirtpath, Utils.getCoolnessFromFilename(shirtpath))).withHat(new FigureElement(hatpath, Utils.getCoolnessFromFilename(hatpath))).withHands(new FigureElement(handpath, Utils.getCoolnessFromFilename(handpath))).withLegs(new FigureElement(legpath, Utils.getCoolnessFromFilename(legpath))).withShoes(new FigureElement(shoepath, Utils.getCoolnessFromFilename(shoepath))).build();
	}

}
