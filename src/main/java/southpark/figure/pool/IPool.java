package southpark.figure.pool;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.BodyPart;

/**
 * Pool interface
 * @param <T> type of pool
 */
public interface IPool<T> {

	/**
	 * Type of body part
	 */
	public BodyPart getType();
	
	/**
	 * Next element of the pool
	 */
	public T getNext();
	
	/**
	 * Previous element of the pool
	 */
	public T getPrevious();
	
	/**
	 * Size of the pool
	 */
	public int getSize();
	
	/**
	 * Fetches images from the directory folder
	 */
	public void reFetchImages();
	
	/**
	 * Get a random figure element from the pool.
	 */
	public FigureElement getRandom();
	
}
