package southpark.figure.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Triangle
 */
public class DrawTriangle extends Drawable implements Cloneable {

	/**
	 * Constructor
	 */
	public DrawTriangle() {
		super();
	}

	/**
	 * Constructor
	 */
	public DrawTriangle(Color foreground, Point point, int size) {
		super(foreground, point, size);
	}

	@Override
	public void drawShape(Graphics g) {
		Point point2 = new Point(location.x + size, location.y);
		Point point3 = new Point(location.x + (size / 2), location.y - size);
		g.drawLine(location.x, location.y, point2.x, point2.y);
		g.drawLine(location.x, location.y, point3.x, point3.y);
		g.drawLine(point2.x, point2.y, point3.x, point3.y);

	}

	@Override
	public Drawable clone() throws CloneNotSupportedException { // NOPMD
		return new DrawTriangle(this.color, this.location, this.size);
	}

	/**
	 * Centerize the drawable to the click
	 */
	@Override
	protected void centerize() {
		double oldX = location.getX();
		double oldY = location.getY();
		double offset = size / 2.0;
		// As we trying to draw equilateral_triangle
		double tempY = oldY + (Math.sqrt(3) / 2) * size;
		this.location = new Point((int) (oldX - offset), (int) (tempY - offset));
	}

}