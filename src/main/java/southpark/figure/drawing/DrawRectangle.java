package southpark.figure.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Draws rectangle
 */
public class DrawRectangle extends Drawable implements Cloneable {
	/**
	 * Constructor
	 */
	public DrawRectangle() {
		super();
	}

	/**
	 * Constructor
	 */
	public DrawRectangle(Color foreground, Point point, int size) {
		super(foreground, point, size);
	}

	@Override
	public void drawShape(Graphics g) {
		g.fillRect(((int) location.getX()), ((int) location.getY()), size, size);

	}

	@Override
	public Drawable clone() throws CloneNotSupportedException { // NOPMD
		return new DrawRectangle(this.color, this.location, this.size);
	}

}
