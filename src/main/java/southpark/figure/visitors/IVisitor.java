package southpark.figure.visitors;

/**
 * Visitor interface
 * 
 * @param <T>
 * 			Type of the element we want to visit
 */
public interface IVisitor<T> {
	
	/**
	 * Visit an element.
	 */
	public void visit(T element);
	
}
