package southpark.figure.visitors;

import southpark.figure.entity.FigureElement;
import southpark.figure.entity.IFigure;
import southpark.figure.enums.BodyPart;
import southpark.figure.pool.BodyPartPool;

import java.util.HashMap;
import java.util.Map;

/**
 * Random body part pool visitor
 * 
 * Creates a map with random body pool elements
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for BodyPartPool
 * 
 */
public class RandomBodyPartPoolVisitor implements IVisitor<BodyPartPool> {

	private Map<BodyPart, IFigure> elements = new HashMap<>();

	/**
	 * Visit.
	 */
	public void visit(BodyPartPool pool) {
		FigureElement bodyPart = pool.getRandom();
		if (bodyPart != null) {
			elements.put(pool.getType(), bodyPart);
		}
	}

	/**
	 * Get elements.
	 */
	public Map<BodyPart, IFigure> getElements() {
		return elements;
	}

}
