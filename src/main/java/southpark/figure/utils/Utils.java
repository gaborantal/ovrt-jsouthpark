package southpark.figure.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JLayeredPane;
import javax.swing.JTextField;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.BodyPart;

/**
 * Util class
 */
public class Utils {
	/**
	 * Utils logger
	 */
	private static final Logger logger = Logger.getLogger(Utils.class.getCanonicalName());

	/**
	 * Random generator
	 */
	private static Random generator = new Random(System.currentTimeMillis());

	/**
	 * Fetch images.
	 */
	public static List<FigureElement> fetchImages(String directory) {
		List<FigureElement> ret = new ArrayList<>();
		List<File> files = (List<File>) FileUtils.listFiles(new File(directory), Constants.SUPPORTED_EXTENSIONS, false);

		files.forEach(currentFile -> {
			int coolnessPoint = getCoolnessFromFilename(currentFile.getName());
			ret.add(new FigureElement(currentFile.getPath(), coolnessPoint));
		});

		return ret;
	}

	/**
	 * Get coolness from file name.
	 */
	public static int getCoolnessFromFilename(String fileName) {
		int returnValue = Constants.DEFAULT_VALUE;
		String name = FilenameUtils.removeExtension(fileName);
		String[] parts = name.split("_");
		try {
			returnValue = Integer.parseInt(parts[parts.length - 1]);
		} catch (NumberFormatException e) {
			logger.error("Cant find coolness point, is set to " + returnValue + "; Filename is: " + name);
		}
		return returnValue;
	}

	/**
	 * Create image.
	 */
	public static BufferedImage createImage(JLayeredPane jLayeredPane) {
		int w = jLayeredPane.getWidth();
		int h = jLayeredPane.getHeight();
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		jLayeredPane.paint(g);
		return bi;
	}

	/**
	 * Get directory.
	 */
	public static String getDirectory(BodyPart part) {
		switch (part) {
			case ACCESSORIES:
				return Constants.ACCESSORIES_DIR;
			case BACKGROUND:
				return Constants.BACKGROUND_DIR;
			case EYES:
				return Constants.EYES_DIR;
			case HAIR:
				return Constants.HAIR_DIR;
			case HANDS:
				return Constants.HANDS_DIR;
			case HAT:
				return Constants.HATS_DIR;
			case LEGS:
				return Constants.LEGS_DIR;
			case MOUTH:
				return Constants.MOUTH_DIR;
			case SHIRT:
				return Constants.SHIRT_DIR;
			case SHOES:
				return Constants.SHOES_DIR;
			case SKIN:
				return Constants.SKIN_DIR;
			case PROPS:
				return Constants.PROPS_DIR;
			default:
				return "Unknown dir";
		}
	}

	/**
	 * Parse integer.
	 */
	public static int parseInteger(JTextField field) {
		int num = Constants.DEFAULT_STEP;
		try {
			num = Integer.parseInt(field.getText());
		} catch (NumberFormatException nfe) {
			logger.error("Wrong value");
		}
		return num;
	}

	/**
	 * Returns random integer in the range 0-num
	 * 
	 * @param i
	 * @return random int
	 */
	public static synchronized int randomInteger(int num) {
		return generator.nextInt(num);
	}

}
