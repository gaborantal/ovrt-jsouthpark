package southpark.figure.entity;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import southpark.figure.enums.Command;
import southpark.figure.gui.ImagePanel;
import southpark.figure.visitors.IVisitable;
import southpark.figure.visitors.IVisitor;

/**
 * Figure element
 */
public class FigureElement implements IFigure, IVisitable<IFigure>, Serializable {

	private static final long serialVersionUID = 1838435264268617127L;

	/**
	 * Visible image of the element. It is transient, because saving this panel
	 * would cause a tragedy (I'm not joking) If the figure is loaded from
	 * .figure file, it has to be null, which is initialized later via
	 * initElementImage() method.
	 */
	private transient ImagePanel elementImage;

	/**
	 * Coolness of an element
	 */
	private int coolness;

	/**
	 * Required, when you want to load a Figure. The elements are serialized to
	 * Json (ImagePanel does not get serialized). After that, if you want to
	 * load a figure, you only need this path to initialize the visible image.
	 */
	private String imagePath;
	
	/**
	 * Image location.
	 * 
	 * The attribute is needed because element image won't be serialized, but its
	 * location needs to be serialized.
	 */
	private Point imageLocation;
	
	/**
	 * Image size.
	 * 
	 * The attribute is needed because element image won't be serialized, but its
	 * size needs to be serialized.
	 */
	private Dimension imageSize;

	/**
	 * Class constructor.
	 */
	public FigureElement(String imagePath, int coolness) {
		elementImage = new ImagePanel(imagePath);
		this.imagePath = imagePath;
		this.coolness = coolness;
		imageLocation = elementImage.getLocation();
		imageSize = elementImage.getSize();
	}

	/**
	 * Class constructor.
	 */
	public FigureElement() {

	}

	/**
	 * Get element image.
	 */
	public ImagePanel getElementImage() {
		return elementImage;
	}
	
	/**
	 * Scale element image size.
	 */
	public void scaleElementImageSize(Command action, int num) {
		// Get current image.
		BufferedImage img = elementImage.getImg();
		// Determine current image size.
		int currentSize = img.getWidth() > img.getHeight() ? img.getWidth() : img.getHeight();
		
		switch (action) {
			case SCALE_UP:
				elementImage.scale(currentSize + num);
				break;
				
			case SCALE_DOWN:
				elementImage.scale((currentSize - num) >= 0 ? currentSize - num : 0);
				break;
				
			default:
				break;
		}
		
		// Update image size.
		imageSize = elementImage.getSize();
	}

	/**
	 * Set element image location.
	 */
	public void setElementImageLocation(Point location) {
		imageLocation = location;
		elementImage.setLocation(imageLocation);
	}

	/**
	 * Get coolness.
	 */
	public int getCoolness() {
		return coolness;
	}

	/**
	 * Accept an IFigure visitor.
	 */
	public void accept(IVisitor<IFigure> v) {
		v.visit(this);
	}

	/**
	 * Get image path.
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * Set image path.
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * Set element image.
	 */
	public void setElementImage(ImagePanel elementImage) {
		this.elementImage = elementImage;
	}

	/**
	 * Set coolness.
	 */
	public void setCoolness(int coolness) {
		this.coolness = coolness;
	}

	/**
	 * Initialize element image.
	 */
	public void initElementImage() {
		if (elementImage == null) {
			elementImage = new ImagePanel(imagePath);
			elementImage.setLocation(imageLocation);
			// Determine current image size.
			int currentSize = imageSize.width > imageSize.height ? imageSize.width : imageSize.height;
			// Scale image.
			elementImage.scale(currentSize);
		}
	}
	
}
