package southpark.figure.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import southpark.figure.enums.BodyPart;
import southpark.figure.gui.ImagePanel;
import southpark.figure.iterator.FigureIterator;
import southpark.figure.observer.IObservable;
import southpark.figure.observer.IObserver;
import southpark.figure.utils.Constants;
import southpark.figure.visitors.AggregateValuesVisitor;
import southpark.figure.visitors.AverageValuesVisitor;
import southpark.figure.visitors.ElementImageVisitor;
import southpark.figure.visitors.IVisitable;
import southpark.figure.visitors.IVisitor;
import southpark.figure.visitors.InitVisibleImageVisitor;
import southpark.figure.visitors.MinMaxValueVisitor;

/**
 * Figure
 */
public class Figure implements IFigure, IVisitable<IFigure>, Serializable, IObservable, Iterable<IFigure> {

	private static final long serialVersionUID = -5704046777167669488L;
	/**
	 * List of elements
	 */
	private List<IFigure> elements;
	/* !!! Do not serialize observers !!! */
	private transient ArrayList<IObserver> observers;
	private transient MinMaxValueVisitor mmvv;

	/**
	 * Class constructor.
	 */
	public Figure() {
		elements = new ArrayList<IFigure>(Constants.PARTS_SIZE);
		observers = new ArrayList<IObserver>();
	}

	/**
	 * Get the image parts.
	 */
	public List<ImagePanel> getImageParts() {
		InitVisibleImageVisitor viv = new InitVisibleImageVisitor();
		elements.forEach(element -> {
			element.accept(viv);
		});
		return viv.getElements();
	}

	/**
	 * Get aggregated coolness.
	 */
	public int getCoolness() {
		AggregateValuesVisitor avv = new AggregateValuesVisitor();
		elements.forEach(element -> {
			element.accept(avv);
		});
		return avv.getValue();
	}

	/**
	 * Get min coolness.
	 */
	public int getMinCoolness() {
		mmvv = new MinMaxValueVisitor();
		elements.forEach(element -> {
			element.accept(mmvv);
		});
		return mmvv.getMinCoolness();
	}

	/**
	 * Get max coolness.
	 */
	public int getMaxCoolness() {
		mmvv = new MinMaxValueVisitor();
		elements.forEach(element -> {
			element.accept(mmvv);
		});
		return mmvv.getMaxCoolness();
	}

	/**
	 * Get minimal item name
	 */
	public String getMinItemName() {
		if (mmvv != null && mmvv.getMin() != null) {
			return mmvv.getMin().getElementImage().getContainerDirectoryPath();
		}
		return "Unknown";
	}

	/**
	 * Get maximal item name
	 */
	public String getMaxItemName() {
		if (mmvv != null && mmvv.getMax() != null) {
			return mmvv.getMax().getElementImage().getContainerDirectoryPath();
		}
		return "Unknown";
	}

	/**
	 * Get average coolness.
	 */
	public double getAvgCoolness() {
		AverageValuesVisitor avv = new AverageValuesVisitor();
		elements.forEach(element -> {
			element.accept(avv);
		});
		return avv.getAvarage();
	}

	/**
	 * Add a figure element.
	 */
	public void add(IFigure figure) {
		elements.add(figure);
		notifyObservers();
	}

	/**
	 * Replace a figure element.
	 */
	public void add(BodyPart whichPart, IFigure figure) {
		int id = whichPart.ordinal();
		// Exception without this.
		if (elements.size() > id && elements.get(id) != null) {
			elements.set(id, figure);
		} else {
			elements.add(id, figure);
		}
		notifyObservers();
	}

	/**
	 * Remove a figure element.
	 */
	public void remove(IFigure figure) {
		elements.remove(figure);
		notifyObservers();
	}

	/**
	 * Accept a visitor.
	 */
	public void accept(IVisitor<IFigure> v) {
		elements.forEach(element -> {
			element.accept(v);
		});
	}

	/**
	 * Get element image.
	 */
	public ImagePanel getElementImage() {
		// There is no need for this.
		throw new NoSuchMethodError("Something bad happened");
	}

	/**
	 * Get elements.
	 */
	public List<IFigure> getElements() {
		return elements;
	}

	/**
	 * Set elements.
	 */
	public void setElements(List<IFigure> elements) {
		this.elements = elements;
	}

	/**
	 * Initialize element image.
	 */
	public void initElementImage() {
		ElementImageVisitor eiv = new ElementImageVisitor();
		elements.forEach(element -> {
			element.accept(eiv);
		});
	}

	/**
	 * Attach an observer.
	 */
	public void attachObserver(IObserver observer) {
		observers.add(observer);
	}

	/**
	 * Detach an observer.
	 */
	public void detachObserver(IObserver observer) {
		for (int i = 0; i < observers.size(); i++) {
			if (observers.get(i) == observer) {
				observers.remove(i);
				break;
			}
		}
	}

	/**
	 * Notify observers.
	 */
	public void notifyObservers() {
		for (int i = 0; i < observers.size(); i++) {
			observers.get(i).update(this);
		}
	}

	/**
	 * Create an iterator for the object.
	 */
	@Override
	public Iterator<IFigure> iterator() {
		return new FigureIterator(this);
	}

}
