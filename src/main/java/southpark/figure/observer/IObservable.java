package southpark.figure.observer;

/**
 * Observable interface
 */
public interface IObservable {
	
	/**
	 * Attach an observer.
	 */
	public void attachObserver(IObserver observer);
	
	/**
	 * Detach an observer.
	 */
	public void detachObserver(IObserver observer);
	
	/**
	 * Notify observers.
	 */
	public void notifyObservers();

}
