package southpark.figure.observer;

/**
 * Observer interface
 */
public interface IObserver {
	
	/**
	 * Update.
	 */
	public void update(IObservable observable);

}
